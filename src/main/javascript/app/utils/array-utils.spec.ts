import ArrayUtils from './array-utils';

describe('ArrayUtils', () => {
    describe('#includes', () => {
        it('should return false if collection is empty', () => {
            const coll = [];
            expect(ArrayUtils.includes(coll, 'some string')).toEqual(false);
        });

        it('should return false if collection is undefined', () => {
            expect(ArrayUtils.includes(undefined, 'some string')).toEqual(false);
        });

        it('should return true if there is query in collection', () => {
            const coll = [1, 4, 6, 1, 6, 7];
            expect(ArrayUtils.includes(coll, 6)).toEqual(true);
        });

        it('should return false if there is not query in collection', () => {
            const coll = [1, 5, 4, 6, 7];
            expect(ArrayUtils.includes(coll, 3)).toEqual(false);
        });

        it('should return true if found in difference collection', () => {
            const coll = [undefined, new Object({}), 'string', 6, null];
            expect(ArrayUtils.includes(coll, 6)).toEqual(true);
        });

        it('should return true if found object in collection', () => {
            const object = { test: 'test' };
            const object2 = { test2: 'test2' };

            const coll = [object, object2];
            expect(ArrayUtils.includes(coll, object)).toEqual(true);
        });
    });
});
