import DateFormatUtils from './date-format-utils';

describe('DateFormatUtils', () => {
    it('should format', () => {
        const difference = 1513108800000 - 1512839357576;
        expect(DateFormatUtils.formatDateDifference(difference)).toEqual('3d 2h 50m');
    });
});