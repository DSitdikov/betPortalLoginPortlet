import { EnvType } from '../enums/env-type';

const env = {
    sportId: 1032,
    refreshInterval: 0,
    requestDelay: 600,
    closeTimeout: 3000,
    apiBaseUrl: '/data',
    apiUrls: {
        league: {
            base: '',
            list: '/real.leaguesEvents.3.json?sportId=:sportId'
        }
    },
    mockRequestMethod: true,
    mockHttpStatusHandler: true
};

if (process.env.NODE_ENV === EnvType.PROD) {
    Object.assign(env, {
        refreshInterval: 0,
        apiBaseUrl: '',
        apiUrls: {
            league: {
                base: '',
                list: '&p_p_resource_id=leaguesEvents'
            }
        },
        mockRequestMethod: false,
        mockHttpStatusHandler: false,
    });
}

export default env;
