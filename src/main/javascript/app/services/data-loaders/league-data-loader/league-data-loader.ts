import env from '../../../env/env';
import League from '../../../entities/domain-model/league/league';
import BasicDataLoader from '../basic-data-loader/basic-data-loader';
import DataLoader from '../common/data-loader';
import LeagueEntities from './league-entities';
import LeagueEvent from '../../../entities/domain-model/league-event/league-event';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import Market from '../../../entities/domain-model/markets/common/market';
import { MarketType } from '../../../entities/domain-model/markets/common/market-type';
import HandicapMarket from '../../../entities/domain-model/markets/handicap-market/handicap-market';
import TotalMarket from '../../../entities/domain-model/markets/total-market/total-market';
import MoneyLineMarket from '../../../entities/domain-model/markets/money-line-market/money-line-market';

export default class LeagueDataLoader {
    loader: DataLoader;

    constructor(data: any = {}) {
        this.loader = data.loader || new BasicDataLoader({ baseApiUrlSuffix: env.apiUrls.league.base });
    }

    getLeagueEntities(sportId: number): Promise<LeagueEntities> {
        const url = env.apiUrls.league.list.replace(':sportId', sportId.toString());
        return this.loader.get(url)
            .then(rawLeagues => {
                const leagues = new Map<number, League>();
                const events = new Map<number, LeagueEvent>();
                const periods = new Map<number, LeagueEventPeriod>();
                const markets = new Map<number, Market>();

                (rawLeagues || []).forEach(league => {
                    leagues.set(league.id, new League(league));

                    (league.events || []).forEach(event => {
                        events.set(event.id, new LeagueEvent({ ...event, leagueId: league.id }));

                        (event.periods || []).forEach(period => {
                            periods.set(period.id, new LeagueEventPeriod({ ...period, eventId: event.id }));

                            this.fillMarketsByPeriod(markets, period);
                        });
                    });
                });

                return {
                    leagues,
                    events,
                    periods,
                    markets
                };
            });
    }

    private fillMarketsByPeriod(markets: Map<number, Market>, rawPeriod: any) {
        (rawPeriod.spreads || []).forEach(market => {
            markets.set(market.id, new HandicapMarket({
                ...market,
                type: MarketType.HANDICAP,
                periodId: rawPeriod.id
            }));
        });

        (rawPeriod.moneylines || []).forEach(market => {
            markets.set(market.id, new MoneyLineMarket({
                ...market,
                type: MarketType.MONEY_LINE,
                periodId: rawPeriod.id
            }));
        });

        (rawPeriod.totals || []).forEach(market => {
            markets.set(market.id, new TotalMarket({
                ...market,
                type: MarketType.TOTALS,
                periodId: rawPeriod.id
            }));
        });

        (rawPeriod.teamTotals || []).forEach(market => {
            markets.set(market.id, new TotalMarket({
                id: market.id,
                originId: market.id,
                type: MarketType.TOTALS,
                periodId: rawPeriod.id,
                over: market.homeOver,
                under: market.homeUnder,
                points: market.homePoints
            }));

            const awayTeamTotalId = TotalMarket.TOTAL_AWAY_ID_FACTOR * market.id;
            markets.set(awayTeamTotalId, new TotalMarket({
                id: awayTeamTotalId,
                originId: market.id,
                type: MarketType.TOTALS,
                periodId: rawPeriod.id,
                over: market.awayOver,
                under: market.awayUnder,
                points: market.awayPoints
            }));
        });
    }
}