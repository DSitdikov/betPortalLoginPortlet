import LeagueEventPeriod from './league-event-period';
import { PeriodType } from './period-type';

describe('LeagueEventPeriod', () => {
    it('should create default instance if constructor parameters are not set', () => {
        const period = new LeagueEventPeriod();

        expect(period.id).toEqual(0);
        expect(period.number).toEqual(PeriodType.FULL_TIME);
        expect(period.teamAwayTotals.length).toEqual(0);
        expect(period.teamHomeTotals.length).toEqual(0);
        expect(period.handicap.length).toEqual(0);
        expect(period.totals.length).toEqual(0);
        expect(period.doubleChance).toEqual(0);
        expect(period.moneyLine).toEqual(0);
        expect(period.bts).toEqual(0);
        expect(period.allTotals.length).toEqual(3);
        expect(period.allTotals[0]).toBe(period.totals);
        expect(period.allTotals[1]).toBe(period.teamHomeTotals);
        expect(period.allTotals[2]).toBe(period.teamAwayTotals);
    });

    it('should parse basic properties', () => {
        const period = new LeagueEventPeriod({
            id: 47365,
            number: 0
        });

        expect(period.id).toEqual(47365);
        expect(period.number).toEqual(PeriodType.FULL_TIME);
    });

    it('should parse money line market', () => {
        const period = new LeagueEventPeriod({
            moneylines: [
                {
                    id: 47376,
                    away: 1.34,
                    home: 10.51,
                    draw: 5.22
                }
            ]
        });

        expect(period.moneyLine).toEqual(47376);
    });

    it('should parse handicap as ordered array (by handicap.hdp ASC)', () => {
        const period = new LeagueEventPeriod({
            spreads: [
                {
                    id: 47368,
                    pinnacleLineId: 5464977135,
                    away: 2.06,
                    home: 1.84,
                    hdp: 1.50
                },
                {
                    id: 47370,
                    pinnacleLineId: 5464977139,
                    away: 1.40,
                    home: 3.01,
                    hdp: 0.75
                },
                {
                    id: 47366,
                    pinnacleLineId: 450840664,
                    away: 1.81,
                    home: 2.10,
                    hdp: 1.25
                },
                {
                    id: 47367,
                    pinnacleLineId: 5464977133,
                    away: 2.39,
                    home: 1.62,
                    hdp: 1.75
                },
                {
                    id: 47369,
                    pinnacleLineId: 5464977137,
                    away: 1.53,
                    home: 2.59,
                    hdp: 1.00
                }
            ]
        });

        expect(period.handicap.length).toEqual(5);
        expect(period.handicap[0]).toEqual(47370);
        expect(period.handicap[1]).toEqual(47369);
        expect(period.handicap[2]).toEqual(47366);
        expect(period.handicap[3]).toEqual(47368);
        expect(period.handicap[4]).toEqual(47367);
    });

    it('should parse totals as ordered array (by total.points ASC)', () => {
        const period = new LeagueEventPeriod({
            totals: [
                {
                    id: 47371,
                    points: 2.75,
                    over: 2.11,
                    under: 1.80
                },
                {
                    id: 47375,
                    points: 3.25,
                    over: 2.80,
                    under: 1.45
                },
                {
                    id: 47373,
                    points: 2.50,
                    over: 1.88,
                    under: 2.02
                },
                {
                    id: 47374,
                    points: 3.00,
                    over: 2.48,
                    under: 1.57
                },
                {
                    id: 47372,
                    points: 2.25,
                    over: 1.64,
                    under: 2.35
                }
            ]
        });

        expect(period.totals.length).toEqual(5);
        expect(period.totals[0]).toEqual(47372);
        expect(period.totals[1]).toEqual(47373);
        expect(period.totals[2]).toEqual(47371);
        expect(period.totals[3]).toEqual(47374);
        expect(period.totals[4]).toEqual(47375);
    });

    it('should parse team (individual) totals', () => {
        const period = new LeagueEventPeriod({
            teamTotals: [
                {
                    id: 47377,
                    pinnacleLineId: 450840664,
                    homePoints: 0.50,
                    homeOver: 2.10,
                    homeUnder: 1.80,
                    awayPoints: 2.00,
                    awayOver: 1.99,
                    awayUnder: 1.90
                }
            ]
        });

        expect(period.teamHomeTotals.length).toEqual(1);
        expect(period.teamHomeTotals[0]).toEqual(47377);
        expect(period.teamAwayTotals.length).toEqual(period.teamHomeTotals.length);
        expect(period.teamAwayTotals[0]).toEqual(-47377);
    });

    it('should prepare array with all totals: overall, home team, away team', () => {
        const period = new LeagueEventPeriod({
            totals: [
                {
                    id: 47371,
                    points: 2.75
                },
                {
                    id: 47375,
                    points: 3.25
                },
                {
                    id: 47373,
                    points: 2.50
                }
            ],
            teamTotals: [
                {
                    id: 47377,
                    homePoints: 0.50,
                    homeOver: 2.10,
                    homeUnder: 1.80,
                    awayPoints: 2.00,
                    awayOver: 1.99,
                    awayUnder: 1.90
                }
            ]
        });

        expect(period.allTotals.length).toEqual(3);
        expect(period.allTotals[0]).toBe(period.totals);
        expect(period.allTotals[1]).toBe(period.teamHomeTotals);
        expect(period.allTotals[2]).toBe(period.teamAwayTotals);
    });
});