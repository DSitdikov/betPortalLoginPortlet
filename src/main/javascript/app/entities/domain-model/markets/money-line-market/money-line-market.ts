import SimpleLineMarket from '../simple-line-market/simple-line-market';
import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';
import { MarketType } from '../common/market-type';

export default class MoneyLineMarket extends SimpleLineMarket {

    constructor(data: any = {}) {
        super({
            ...data,
            type: MarketType.MONEY_LINE
        });
    }

    protected selectOutcomeName(outcomeType: string, event: LeagueEvent): string {
        if (outcomeType === OutcomeType.DRAW) {
            return 'Draw';
        }

        const isHome = outcomeType === OutcomeType.HOME;
        const team = isHome ? event.home : event.away;
        return `${team} ML`;
    }
}
