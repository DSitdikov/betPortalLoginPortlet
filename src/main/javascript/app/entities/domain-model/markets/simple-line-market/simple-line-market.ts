import Price from '../../price/price';
import Market from '../common/market';
import { OutcomeType } from '../../outcome/outcome-type';

export default abstract class SimpleLineMarket extends Market {
    protected outcomeTypes: string[] = [OutcomeType.HOME, OutcomeType.AWAY, OutcomeType.DRAW];
    draw: Price;

    constructor(data: any = {}) {
        super(data);

        this.draw = new Price(data.draw);
    }
}
