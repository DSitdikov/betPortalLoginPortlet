import Price from '../../price/price';
import { MarketConstants } from '../../../../constants/market.constants';
import Market from '../common/market';
import { MarketType } from '../common/market-type';
import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';

export default class TotalMarket extends Market {
    static TOTAL_AWAY_ID_FACTOR = -1;

    protected outcomeTypes: string[] = [OutcomeType.OVER, OutcomeType.UNDER];
    points: number;

    constructor(data: any = {}) {
        super({
            ...data,
            type: MarketType.TOTALS
        });

        this.points = data.points || 0;
        const pointsFormatted = this.points.toFixed(MarketConstants.SIZE_OF_DECIMAL_PART);
        this.outcomeA = new Price({
            tip: `Over ${pointsFormatted}`,
            value: typeof data.outcomeA === 'object' ? data.outcomeA.value : data.over
        });
        this.outcomeB = new Price({
            tip: `Under ${pointsFormatted}`,
            value: typeof data.outcomeB === 'object' ? data.outcomeB.value : data.under
        });
    }

    protected selectOutcomeName(outcomeType: string, event: LeagueEvent): string {
        return outcomeType === OutcomeType.OVER ? this.outcomeA.tip : this.outcomeB.tip;
    }
}
