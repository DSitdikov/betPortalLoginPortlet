import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';
import DoubleChanceMarket from './double-chance-market';

describe('DoubleChanceMarket', () => {
    it('should create default instance', () => {
        const market = new DoubleChanceMarket();

        expect(market.id).toEqual(0);
        expect(market.originId).toEqual(0);
        expect(market.periodId).toEqual(0);
        expect(market.type).toEqual('DC');
        expect(market.outcomeB.formatted).toEqual('No line');
        expect(market.outcomeA.formatted).toEqual('No line');
        expect(market.draw.formatted).toEqual('No line');
    });

    it('should parse raw market', () => {
        const market = new DoubleChanceMarket({
            id: 47544,
            periodId: 33121,
            away: 1.3,
            home: 10.518,
            draw: 0
        });

        expect(market.id).toEqual(47544);
        expect(market.originId).toEqual(47544);
        expect(market.periodId).toEqual(33121);
        expect(market.type).toEqual('DC');
        expect(market.outcomeA.formatted).toEqual('10.52');
        expect(market.outcomeB.formatted).toEqual('1.30');
        expect(market.draw.formatted).toEqual('No line');
    });

    it('should return default outcome name if outcome type is undefined or wrong', () => {
        const market = new DoubleChanceMarket();
        const event = new LeagueEvent();

        expect(market.getOutcomeName('random value', event)).toEqual('');
        expect(market.getOutcomeName('', event)).toEqual('');
    });

    it('should return outcome name if outcome type is outcomeA', () => {
        const event = new LeagueEvent({
            home: 'Huddersfield Town'
        });

        const market = new DoubleChanceMarket();
        expect(market.getOutcomeName(OutcomeType.HOME, event)).toEqual('2X (Huddersfield Town ML or Draw)');
    });

    it('should return outcome name if outcome type is outcomeB', () => {
        const event = new LeagueEvent({
            away: 'Chelsea'
        });

        const market = new DoubleChanceMarket();
        expect(market.getOutcomeName(OutcomeType.AWAY, event)).toEqual('1X (Chelsea ML or Draw)');
    });

    it('should return outcome name if outcome type is draw', () => {
        const event = new LeagueEvent({
            home: 'Huddersfield Town',
            away: 'Chelsea'
        });

        const market = new DoubleChanceMarket();
        expect(market.getOutcomeName(OutcomeType.DRAW, event)).toEqual('12 (Huddersfield Town ML or Chelsea ML)');
    });
});