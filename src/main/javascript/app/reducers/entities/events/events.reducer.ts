import CommonAction from '../../../actions/common/types/common-action';
import LeagueEvent from '../../../entities/domain-model/league-event/league-event';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

const initialState = new Map<number, LeagueEvent>();

export default function eventsReducer(state: Map<number, LeagueEvent> = initialState, action: CommonAction) {
    if (action.type === DomainModelActionType.FETCH_LEAGUES_SUCCESS) {
        const leagueEntities = action.payload as LeagueEntities;
        return leagueEntities.events;
    }
    return state;
}