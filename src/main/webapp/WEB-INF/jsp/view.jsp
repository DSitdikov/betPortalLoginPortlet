<%@ include file="/WEB-INF/jsp/init.jsp" %>

<c:choose>
    <c:when test="<%= themeDisplay.isSignedIn() %>">

        <%
            String signedInAs = HtmlUtil.escape(user.getFullName());

            if (themeDisplay.isShowMyAccountIcon() && (themeDisplay.getURLMyAccount() != null)) {
                String myAccountURL = String.valueOf(themeDisplay.getURLMyAccount());

                signedInAs = "<a class=\"signed-in\" href=\"" + HtmlUtil.escape(myAccountURL) + "\">" + signedInAs + "</a>";
            }
        %>

        <liferay-ui:message arguments="<%= signedInAs %>" key="you-are-signed-in-as-x" translateArguments="<%= false %>" />
    </c:when>
    <c:otherwise>

        <%
            String redirect = ParamUtil.getString(request, "redirect");
        %>

        <portlet:actionURL name="/login/login" var="loginURL">
            <portlet:param name="action" value="/login/login" />
        </portlet:actionURL>

        <form class="a-login-form" action="<%= loginURL %>" method="post" name="loginForm" autocomplete="on">
            <input name="saveLastPath" type="hidden" value="<%= false %>">
            <input name="redirect" type="hidden" value="<%= redirect %>"/>
            <div class="a-login-form__heading">Log In</div>
            <div class="a-login-form__description">Please enter your personal data for authorization.</div>
            <input value="" name="login" placeholder="Username" class="a-login-form__input">
            <input value="" name="password" placeholder="Password" class="a-login-form__input a-login-form__input_type_password" type="password">
            <div class="a-login-form__remember-me">
                <div class="a-login-form__remember-me-title">Remember me</div>
                <div class="a-login-form__remember-me-switcher">
                    <div class="a-switcher">
                        <div class="a-switcher__handle"></div>
                    </div>
                </div>
            </div>
            <%
                boolean error = "true".equals(ParamUtil.getString(request, "error"));
                String errorMessage = ParamUtil.getString(request, "errorMessage");
                String errorCode = ParamUtil.getString(request, "errorCode");
            %>
            <c:set var="error" scope="session" value="<%= error %>" />
            <c:set var="errorMessage" scope="session" value="<%= errorMessage %>" />
            <c:set var="errorCode" scope="session" value="<%= errorCode %>" />
            <div class="a-login-form__alert${error ? ' a-login-form__alert_showed' : ''}">
                <%= errorMessage %>
                <br>
                (Error code: <%= errorCode %>)
            </div>
            <div class="a-login-form__btn-submit">
                <button type="submit" class="a-btn">
                    <span class="a-btn__title">Log in</span>
                    <div class="a-btn__loader"></div>
                </button>
            </div>
        </form>
    </c:otherwise>
</c:choose>
