package softpro.loginportlet.spring.portlet;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.UserEmailAddressException;
import com.liferay.portal.kernel.exception.UserPasswordException;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.AuthException;
import com.liferay.portal.kernel.security.auth.session.AuthenticatedSessionManagerUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import softpro.loginportlet.spring.services.PriceProcessorService;

import static com.liferay.portal.kernel.util.PortalUtil.getPortletId;

/**
 * @author softpro
 */
@Controller
@RequestMapping("VIEW")
public class LoginPortletViewController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginPortletViewController.class);

    @Autowired
    PriceProcessorService priceProcessorService;

    @RenderMapping
    public String view(RenderRequest request, RenderResponse response, ModelMap model) {
        User user = (User) request.getAttribute(WebKeys.USER);
        String userScreenName = user != null ? user.getScreenName() : "anonymous";

        ResourceURL baseResourceUrl = response.createResourceURL();

        model.addAttribute("portletResourceUrl", baseResourceUrl.toString());
        model.addAttribute("standalone", false);
        model.addAttribute("authenticatedUser", userScreenName);
        model.addAttribute("portletId", getPortletId(request));
        model.addAttribute("portletAppContextPath", request.getContextPath());

        return "view";
    }

    @ActionMapping(params = "action=/login/login")
    public void login(ActionRequest actionRequest,
                    ActionResponse actionResponse) throws Exception {

        ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

        HttpServletRequest request = PortalUtil.getOriginalServletRequest(
                PortalUtil.getHttpServletRequest(actionRequest));

        HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);

        String login = ParamUtil.getString(actionRequest, "login").toLowerCase();
        String password = actionRequest.getParameter("password");
        boolean rememberMe = ParamUtil.getBoolean(actionRequest, "rememberMe");
        String authType = CompanyConstants.AUTH_TYPE_SN;

        try {
            AuthenticatedSessionManagerUtil.login(request, response, login, password, rememberMe, authType);
            actionResponse.sendRedirect(themeDisplay.getPathMain());
        } catch (AuthException | NoSuchUserException e) {
            actionResponse.setRenderParameter("error", "true");
            actionResponse.setRenderParameter("errorMessage", "Unable to log in with provided credentials.");
            actionResponse.setRenderParameter("errorCode", "3");
        } catch (UserEmailAddressException e) {
            actionResponse.setRenderParameter("error", "true");
            actionResponse.setRenderParameter("errorMessage", "Login must not be null.");
            actionResponse.setRenderParameter("errorCode", "4");
        } catch (UserPasswordException e) {
            actionResponse.setRenderParameter("error", "true");
            actionResponse.setRenderParameter("errorMessage", "Password must not be null.");
            actionResponse.setRenderParameter("errorCode", "5");
        }
    }

    @ResourceMapping("hello")
    public void hello(ResourceResponse response) throws Exception {
        LOGGER.debug("called quickstart method");

        String eventJson = this.priceProcessorService.getEvents();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getPortletOutputStream().write(eventJson.getBytes());
    }
}